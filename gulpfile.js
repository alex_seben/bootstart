/* global require */
/* Ajustar o arquivo package.json para os dados do GULP! packageGULP.json */
var pkg = require('./package.json');
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');
var header = require('gulp-header');
var autoprefixer = require('gulp-autoprefixer');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

var now = new Date();
var date = [
	now.getMonth() + 1,
	now.getDate(),
	now.getFullYear()
].join('/');

var banner = ['/*!',
' * <%= pkg.title || pkg.name %> - <%= pkg.description %>',
' * @version v<%= pkg.version %>',
' * @link <%= pkg.homepage %>',
' * @license <%= pkg.license %>',
' * @copyright (c) <%= date %> <%= pkg.author.name %> (<%= pkg.author.url %>)',
' */',
''].join('\n');

var data = {
	pkg : pkg,
	date: date
};

gulp.task('concat', function() {
	gulp.src('src/js/*.js')
	.pipe(concat('main.js'))
	.pipe(gulp.dest('js/'));
});

gulp.task('cssmin', function () {
	gulp.src('css/main.css')
	.pipe(cssmin())
	.pipe(header(banner, data))
	.pipe(concat('main.min.css'))
	.pipe(gulp.dest('css/'));
});

gulp.task('sass', function () {
	gulp.src('src/css/main.scss')
	.pipe(sass())
	.pipe(concat('main.css'))
	.pipe(gulp.dest('css/'));
});

gulp.task('autoprefixer', function () {
	gulp.src('css/main.css')
	.pipe(autoprefixer({
		browsers: ['last 4 versions', 'ie 8', 'ie 9'],
		cascade: false,
		remove: false // mantém prefixos não utilizados
	}))
	.pipe(concat('main.css'))
	.pipe(gulp.dest('css/'));
});

gulp.task('uglify', function() {
	gulp.src('js/main.js')
	.pipe(uglify())
	.pipe(header(banner, data))
	.pipe(concat('main.min.js'))
	.pipe(gulp.dest('js/'));
});

gulp.task('imagemin', function () {
    gulp.src('img/*')
	.pipe(imagemin({
		progressive: true,
		svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
	}))
	.pipe(gulp.dest('img'));
});

gulp.task('watch', function() {
	livereload.listen();
	gulp.watch('**/*.{html,php}', []);
	gulp.watch('src/css/*.scss', ['sass']);
	gulp.watch('src/js/*.js', ['concat']);
});

gulp.task('default', ['sass', 'concat'], function() {
  // fired before 'finished' event
});

gulp.task('build', ['sass', 'autoprefixer', 'cssmin', 'concat', 'uglify'], function() {
  // fired before 'finished' event
});